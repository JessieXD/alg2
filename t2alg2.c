#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>
#include <string.h>

//InicHeap (inicia o heap)
typedef struct {
    int * A; 
    int tamanhoAtual;
    int tamanhoMaximo;
} HEAP; 

struct paciente {
    char nome[20];
    int prioridade;
};
 
typedef struct paciente pacientes;
pacientes vetor[10];

void inicializarHeap(HEAP * h, int tamanhoMax){
  h->A = (int*) malloc(sizeof(int)*(tamanhoMax+1));
  h->tamanhoAtual = 0;
  h->tamanhoMaximo = tamanhoMax;
}

//realiza qualquer troca entre componentes de vetor
void trocaPosicao (int n1, int n2) {
    pacientes aux = vetor[n1];
    vetor [n1] = vetor[n2]; 
    vetor[n2] = aux;  
}

//Insereheap (insere algo no heap)
void ordenaheap (int tam) {
    int novo=tam + 1; 
    while (novo>0 && vetor[novo/2].prioridade<vetor[novo].prioridade){
        trocaPosicao(novo, novo/2);
        novo = novo/2;
    }
}

void Heapfy (int tam) { 
    int i;
    for (i=0; i<tam; i++) 
        ordenaheap(i);
}

//Removeheap (remove algo do heap)
void Removeheap (int tam){
    int i; 
    for (i=0; i<=tam-1; i++)
        vetor[i]=vetor[i+1];
    Heapfy(tam);
}

//Checaheap (checa se o vetor é uma heap)
int Checaheap (int tam){
    int i; 
    for (i=tam; i>=2; i--){
        if (vetor[i/2].prioridade<vetor[i].prioridade)
            return 0; //nao e heap
        return 1; //e heap
    }
}

void mostraPaciente(int posicao){
    printf ("Nome: %s \nPrioridade: %i \n", vetor[posicao].nome, vetor[posicao].prioridade);
}

//ImprimeHeap (imprime o heap)
void Imprime (int tam) {
    int i; 
    for (i=0; i<=tam; i++){
        printf("id: %i \n", i);
        mostraPaciente(i);
        printf ("\n"); 
    }
}


void SacodeHeap (int tam){
    int i=2; 
    while (i <= tam) {
        if (i<tam && vetor[i].prioridade<vetor[i+1].prioridade)
            i++;
        if (vetor[i/2].prioridade > vetor[i].prioridade)
            break;
        trocaPosicao (i, i/2); 
        i = i*2; 
    }
}  

/*HeapSort
void HeapSort (int tam){
    int i; 
    //for pra fazer o max heapify de tam /2 - 1 até 0
    Heapfy(tam); 

    //for para o sort
    for (i=tam; i>1; i--)
        trocaPosicao (1, i);
        Heapfy(i-1);
}*/ 

/*void HeapSort(int tam){
    int i;
    for (i = tam/2 -1; i >=0; i--)
        Heapfy(i);
    for (i=tam-1; i>=0; i--){
        trocaPosicao(0, i);
        Heapfy(i);
    }
}*/

/*
void SacodeHeap (int tam){
    int pai = 1, filho = 2, aux = vetor[0].prioridade; 
    while (filho <= tam){
        if (filho < tam && vetor[filho].prioridade < vetor[filho+1].prioridade) ++filho; 
        if (aux >= vetor[filho].prioridade) break;
        vetor[pai] = vetor[filho];
        pai = filho; 
        filho = 2*pai; 
    }
    vetor[pai].prioridade = aux; 
} */

void HeapSort (int tam){
    int i;
    for (i=tam; i>=0; --i) {
        trocaPosicao(1,i);
        SacodeHeap(i-1);
    }
}

//Alteraheap (altera a prioridade)
void Alteraheap (int posicao, int novo, int tam){
    vetor[posicao].prioridade = novo; 
    Heapfy (tam);
}

void cadastraPaciente(int posicao){
    printf ("Nome: ");
    scanf ("%s", vetor[posicao].nome);
    
    printf ("Prioridade: ");
    scanf ("%i", &vetor[posicao].prioridade);
}

int main () {

    int id, prioridade;
    int qtdPacientes = -1;
    int qtdTotal     = 0;
    int posicao      = 0;
    int acao         = 0;
    int i;

    srand(time(NULL)); 
    //vetor de nomes utilizado para aleatorizar os nomes no teste
    char *nomes_teste[32][20]; 
    nomes_teste[0][20] = "Ana";
    nomes_teste[1][20] = "Amanda";
    nomes_teste[2][20] = "Alice";
    nomes_teste[3][20] = "Bianca";
    nomes_teste[4][20] = "Bruna";
    nomes_teste[5][20] = "Brenda";
    nomes_teste[6][20] = "Carolina";
    nomes_teste[7][20] = "Caliope";
    nomes_teste[8][20] = "Daiana";
    nomes_teste[9][20] = "Donna";
    nomes_teste[10][20] = "Fabiana";
    nomes_teste[11][20] = "Gabriela";
    nomes_teste[12][20] = "Helena";
    nomes_teste[13][20] = "Ivone";
    nomes_teste[14][20] = "Jovana";
    nomes_teste[15][20] = "Karol";
    nomes_teste[16][20] = "Leticia";
    nomes_teste[17][20] = "Marcia";
    nomes_teste[18][20] = "Marcela";
    nomes_teste[19][20] = "Manuela";
    nomes_teste[20][20] = "Natalia";
    nomes_teste[21][20] = "Otavia";
    nomes_teste[22][20] = "Patrica";
    nomes_teste[23][20] = "Penelope";
    nomes_teste[24][20] = "Renata";
    nomes_teste[25][20] = "Rosangela";
    nomes_teste[26][20] = "Sabrina";
    nomes_teste[27][20] = "Samantha";
    nomes_teste[28][20] = "Taina";
    nomes_teste[29][20] = "Tiana";
    nomes_teste[30][20] = "Viviane";
    nomes_teste[31][20] = "Valeria";
    nomes_teste[32][20] = "Wanessa";

    while (acao != 7) {
        printf ("Olá Madame Estraela! O que deseja fazer?\n");
        printf (" 1 para cadastrar um paciente\n 2 para chamar um paciente para atendimento\n 3 para atualizar prioridade de alguem\n 4 para ordenar a fila de prioridade\n 5 para imprimir a fila de prioridade\n 6 para realizar um teste\n 7 para finalizar o turno\n"); 
        /*
        1 para cadastrar um paciente
        2 para chamar um paciente para atendimento
        3 para atualizar prioridade de alguem
        4 para ordenar a fila de prioridade
        5 para imprimir a fila de prioridade
        6 para realizar um teste
        7 para finalizar o turno. 
        */
        scanf ("%i", &acao); 

        switch (acao){
            case 1:
                //cadastra o paciente, insere ele no heap usando o Heapfy
                qtdPacientes++;
                qtdTotal++;
                cadastraPaciente(qtdPacientes);
                Heapfy(qtdPacientes);
                                
                break;                    
            case 2:
                //remove o primeiro da fila de prioridade
                Removeheap(qtdPacientes);
                qtdPacientes--;
                                                
                break;                    
            case 3:
                //imprime a fila, pede para o usuário inserir o ID
                Imprime(i);
                printf ("selecione o id do paciente a ser alterado\n"); 
                scanf ("%i", &id);
                printf ("Qual é a nova prioridade?\n");
                scanf ("%i", &prioridade);
                Alteraheap(id, prioridade, qtdPacientes); //realiza a troca
                Imprime(qtdPacientes); //imprime o resultado 
            
                break;
            case 4:
                //ordena a fila usando HeapSort e imprime o resultado
                HeapSort(qtdPacientes);
                printf("depois de ordenar\n");
                Imprime (qtdPacientes);
                           
                break;                    
            case 5:
                //imprime fila de prioridade
                Imprime(qtdPacientes);
                            
                break;                    
            case 6:
                //utiliza a função de randomização para gerar nomes e prioridades
                for (i=0; i<=32; i++){
                    strcpy(vetor[i].nome, nomes_teste[rand()%32][20]);
                    vetor[i].prioridade = rand()%100; 
                    qtdPacientes = 32;
                    qtdTotal = 32; 
                    Heapfy(qtdPacientes); //cria um heap com o vetor criado
                }
                            
                break;                    
            case 7:
                //finaliza o turno imprimindo o numero total de pacientes atendidos
                printf ("O turno chegou ao fim! Hoje foram atendidos %i pacientes\n", qtdTotal);           

                break;                    
            default:
                printf ("Numero digitado nao valido! \n");
        }

    } //while principal
} //main
